def solution(A, K):
    if (len(A) <= 0):
        return A
        
    i = 0

    while i < K:
        newArr = []
        newArr.append(A[len(A) - 1])
        newArr.extend(A[:len(A) - 1])
        A = newArr
        i += 1    
        
    return A

print(solution([3, 8, 9, 7, 6], 3) ==  [9, 7, 6, 3, 8])
print(solution([0, 0, 0], 1) ==  [0, 0, 0])
print(solution([], 4) == [])