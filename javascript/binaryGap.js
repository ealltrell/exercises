const solution = N => { 
	if (N < 0) return;
	let binaryArr = (N).toString(2).split('1');
	let binaryGap = [];
	
	binaryArr.forEach( (gap, idx) => {
		if (gap === '') return;
		else {
			let nextIdx = idx + 1;
			if (!binaryArr[nextIdx]) {
			    return;
			}
			binaryGap.push(gap.length);
		}
	})

	return binaryGap.length > 0 ? Math.max(...binaryGap) : 0;
}

console.log(solution(9));
console.log(solution(542));
