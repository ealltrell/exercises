/*
 * This is my solution to a little coding challenge. The challenge
 * was to write a function that, when passed a string of parentheses,
 * eg "()))(", will check for any unmatched parentheses, add to the 
 * beginning or end of the string as needed to "complete" it, and return 
 * a corrected string.
*/

const solution = (parentheses) => {
    /*  
     *  1. Split parentheses into array 
     *  2. Create empty object to store unmatched parenthesis
     *  3. Create empty array to store matched parentheses indexes in the 
     *     parenArr array 
     *  4. Create dict of functions for checking what should be added to 
     *     the parenArr before joining and returning
    */
    let parenArr = parentheses.split('');
    let addObj = {};
    let ignoreList = [];
    let correctStringDict = {
        "(": () => parenArr.push(')'),
        ")": () => parenArr.unshift('(')
    }

    /* 
     * This function  is used by the isMatchingPair function. 
     * It takes an index of the parenArr array to check against 
     * the ignoreList array, and an offSet value which is used to 
     * when adding values to the ignore list so that it can skip 
     * over nested parenthesis that have already been ignored.
     * It returns an array with an index for checking the next value
     * in the parenArr array in the isMatchingPair function in
     * position 0 and an offset value to be passed back to the for 
     * loop so that the appropriate number of nested (and already 
     * ignored) parentheses can be skipped when adding new values to
     * the ignoreList array. 
     * It checks to see if an index should be ignored, if so it
     * looks for the next non-ignored parenthesis in the parenArr
     * array and iterates the offSet value until it finds a non-ignored
     * parenthesis.
    */
    const checkIgnore = (idx, offSet) => {
        if (ignoreList.includes(idx + 1)) {
            return checkIgnore(idx + 1, offSet + 1);
        }
        return [idx, offSet];
    }

    /* 
     * This function checks to see if an opening parenthesis (eg, "(") 
     * has a matching closing parenthesis (eg, ")"). It takes an index
     * value to check in the parenArr array. It uses the checkIgnore
     * function above to skip over any nested parentheses that have 
     * already been ignored. It returns an array with a bool for whether 
     * the orginal index passed to it has a matching parenthesis in the 
     * parenArr array, and the offSet value established and passed up 
     * by the checkIgnore function.
    */
    const isMatchingPair = (idx) => {
        let check = checkIgnore(idx, 1);
        idx = check[0];
        return parenArr[idx + 1] === ')' ? [true, check[1]] : [false, check[1]];
    }

    /* 
     * This for loop iterates through the parenArr array and adds
     * indexes for matching parentheses to an ignoreList array. When it 
     * finds an item to be ignored, it starts over to account for nested 
     * parentheses. If an item has no matching parenthesis (and thus has 
     * not been ignored), it gets added to an addObj as { index: [parenthesis] }
     * which will be used later to add parenthesis onto the front or back of the 
     * parenArr as needed. Once there are no more matching values, it exits. 
    */
    for (let i = 0; i < parenArr.length; i++) {
        if (ignoreList.includes(i)) {
            continue;
        }
        let isMatch = isMatchingPair(i);
        if (parenArr[i] === '(' && isMatch[0]) {
            ignoreList.push(i, i + isMatch[1]);
            i = 0;
            continue;
        }
        addObj[i] = (parenArr[i]);
    }

    /* 
     * This forEach goes through the keys in the addObj object to determine
     * whether parenthesis should be added to the parenArr and adds to the 
     * front or back of the array accordingly. 
    */ 
    Object.keys(addObj).forEach( (idx) => {
        if (ignoreList.includes(Number(idx))) {
            return;
        }
        
        correctStringDict[addObj[idx]]()
    })
    
    /*
     * Return the parenArr joined back together as a string
    */
    return parenArr.join('');
}

/* 
 * A quick and dirty solution for running tests, the first item in each
 * array is the value to pass to the solution, the second item is the 
 * expected output.
*/
let tests = [
    ['(()(', '(()())'], 
    ['(', '()'], 
    ['))', '(())'], 
    ['()))(', '((()))()'], 
    [')()))(', '((()()))()'], 
    ['(((())(()', '(((())(())))'], 
    ['((((((', '(((((())))))'],
    ['())))())))', '((((((())))())))'] 
]

/* 
 * The "test runner"
*/
tests.forEach( (test, idx) => {
    console.log(
        '\nTest Case: ' + (idx + 1) + 
        '\n\nOriginal:\n\t', test[0], 
        '\n\nExpected:\n\t', test[1], 
        "\n\nActual:\n\t", solution(test[0]), 
        '\n\nMatch:\n\t', solution(test[0]) === test[1]
    );
})