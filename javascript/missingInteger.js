function solution(A) {
    let max = Math.max(...A);
    if (max < 0) return 1;
    let missingInt = 1;

    for (let i = 1; i <= max; i++) {
        if (A.indexOf(i) === -1) {
            missingInt = i;
            break;
        }
        if (A.indexOf(i) > -1) {
            if (i === A.length) {
                missingInt = A.length + 1;
                break;
            }
            continue;
        }
        missingInt = i;
        break;
    }

    return missingInt;
}

let tests = [
    [-1, 2, 3, 5, -2],
    [1, 2, 3]
]

tests.forEach( test => {
    console.log(solution(test));
})