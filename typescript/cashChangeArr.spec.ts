import cashChangeArr from './cashChangeArr';

test('converts \'.75\' to change value array: [0, 0, 0, 0, 0, 0, 3, 0, 0, 0]', () => {
  expect(cashChangeArr('.75')).toEqual([0, 0, 0, 0, 0, 0, 3, 0, 0, 0])
});

test('converts \'2.86\' to change value array: [0, 0, 0, 0, 0, 2, 3, 1, 0, 1]', () => {
  expect(cashChangeArr('2.86')).toEqual([0, 0, 0, 0, 0, 2, 3, 1, 0, 1])
});