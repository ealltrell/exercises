export default function cashChangeArr(input: string): number[] {
  let numInput: number = Number(input);
  let returnArr = [];
  const valObj: object = {
    0: 100.0,
    1: 50.0,
    2: 20.0,
    3: 10.0,
    4: 5.0,
    5: 1.0,
    6: 0.25,
    7: 0.1,
    8: 0.05,
    9: 0.01
  }

  interface CalcObj {
    num: number,
    count: number
  }

  let calc = function(num: number, countVal: number): CalcObj {
    let count: number = 0;
    while (num >= countVal) {
        count++;
        num = ((num * 100) - (countVal * 100)) / 100;
    }
    return {
        num: num,
        count: count
    }
  }

  Object.keys(valObj).forEach( key => {
    let idx: number = Number(key);
    let calcObj: CalcObj = calc(numInput, valObj[idx]);
    returnArr[idx] = calcObj.count;
    numInput = calcObj.num;
  })

  return returnArr;
}

console.log(cashChangeArr('2.86'));